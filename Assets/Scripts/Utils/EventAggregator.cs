﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Data.EventData;
using UnityEngine;

public static class EventAggregator 
{
    private static Dictionary<Type, object> _eventListeners = new Dictionary<Type, object>();

    public static void AddListener<T>(Action<T> action) where T : EventData
    {
        var type = typeof(T);
        if (!_eventListeners.ContainsKey(type))
        {
            _eventListeners[type] = new EventListener<T>();
        }

        var listener = (EventListener<T>)_eventListeners[type];
        listener.Actions.Add(action);
    }

    public static void SendMassage<T>(T massage) where T : EventData
    {
        var type = typeof(T);
        if (!_eventListeners.ContainsKey(type))
            return;

        var listener = (EventListener<T>)_eventListeners[type];
        listener.HandleMassage(massage);
    }

    public static void RemoveListener<T>(Action<T> action) where T : EventData
    {
        var type = typeof(T);
        if (!_eventListeners.ContainsKey(type))
            return;

        var listener = (EventListener<T>)_eventListeners[type];
        listener.Actions.Remove(action);
    }
}
