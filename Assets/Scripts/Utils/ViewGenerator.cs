﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Assets.Scripts.Utils
{
    public static class ViewGenerator
    {
        private static Dictionary<string, MonoBehaviour> _examples = new Dictionary<string, MonoBehaviour>();
        public static T CreateView<T>(string prefabName, Vector2 position, Transform holder) where T : MonoBehaviour
        {
            if (!_examples.ContainsKey(prefabName))
            {
                _examples[prefabName] = Resources.Load<T>(Path.Combine("Prefabs", prefabName));
            }

            return GameObject.Instantiate<T>(_examples[prefabName] as T, position, Quaternion.identity, holder);
        }

        public static GameObject CreateObjectFromResouces(string prefabName)
        {
            return GameObject.Instantiate<GameObject>(Resources.Load<GameObject>(Path.Combine("Prefabs", prefabName)));

        }

        public static Transform CreateHolder(string name)
        {
            return  new GameObject(name).transform;
        }

    }
}