﻿using Assets.Scripts.Data.EventData;
using Assets.Scripts.View;
using TMPro.EditorUtilities;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Managers
{
    public class GameUIManager : MonoBehaviour
    {
        [SerializeField] private Button _buttonStart;
        [SerializeField] private PressingButton _buttonLeft;
        [SerializeField] private PressingButton _buttonRight;

        private InputManager _inputManager;

        public void Initialization(InputManager inputManager)
        {
            _inputManager = inputManager;
        }
        void Start()
        {
            _buttonStart.onClick.AddListener(StartButtonClicked);
            _buttonLeft.ButtonPressAction = LeftButtonPress;
            _buttonLeft.ButtonUpAction = LeftButtonUp;
            _buttonRight.ButtonPressAction = RightButtonPress;
            _buttonRight.ButtonUpAction = RightButtonUp;
        }

        private void StartButtonClicked()
        {
            EventAggregator.SendMassage(new GameStartedEvent());
            _buttonStart.gameObject.SetActive(false);
        }

        private void LeftButtonPress()
        {
            _inputManager.ForceLeft = true;
        }

        private void RightButtonPress()
        {
            _inputManager.ForceRight = true;
        }
        private void LeftButtonUp()
        {
            _inputManager.ForceLeft = false;
        }

        private void RightButtonUp()
        {
            _inputManager.ForceRight = false;
        }
    }
}
