﻿using System;
using UnityEngine;

namespace Assets.Scripts.Managers
{
    public class InputManager
    {
        public bool HasLeft { get; private set; }
        public bool HasRight { get; private set; }

        public bool ForceLeft;
        public bool ForceRight;

        public void OnUpdate()
        {
            HasLeft = false;
            HasRight = false;
            
            HasLeft = Input.GetKey(KeyCode.LeftArrow) || ForceLeft;
            HasRight = Input.GetKey(KeyCode.RightArrow) || ForceRight;
        }
    }
}