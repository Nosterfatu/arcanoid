﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Managers;
using UnityEngine;

public class GameInitializeManager : MonoBehaviour
{
    private InputManager _inputManager;
    private PlayerManager _playerManager;
    private GameRulesManager _gameRulesManager;
    private BrickManager _brickManager;
    [SerializeField] private GameUIManager _uiManager;
    void Start()
    {
        _gameRulesManager = new GameRulesManager();
        _brickManager = new BrickManager(_gameRulesManager);
        _inputManager = new InputManager();
        _playerManager = new PlayerManager(_gameRulesManager, _inputManager);
        _uiManager.Initialization(_inputManager);
    }

    void Update()
    {
        _inputManager.OnUpdate();
        _playerManager.OnUpdate();
    }

    void OnDestroy()
    {
        _playerManager.Unsubscribe();
        _gameRulesManager.Unsubscribe();
        _brickManager.Unsubscribe();
    }
}
