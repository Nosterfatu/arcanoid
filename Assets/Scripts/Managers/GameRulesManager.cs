﻿using Assets.Scripts.Data;
using Assets.Scripts.Data.BrickBonuses;
using Assets.Scripts.Data.EventData;
using Assets.Scripts.Utils;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.Managers
{
    public class GameRulesManager
    {
        public GameRules GameRules { get; private set; }

        private const int DefaultHight = 5;
        private const int DefaultWidth = 15;
        public const float LeftBorderPosition = -1;
        public const float RightBorderPosition = 15;
        public const int PlayerMaxSize = 9;
        public const int PlayerMinSize = 1;

        public GameRulesManager()
        {
             LoadGameRules();
             Subscribe();
        }

        public void Unsubscribe()
        {
            EventAggregator.RemoveListener<BallDestroyedEvent>(BallDestroyed);
        }

        public void Subscribe()
        {
            EventAggregator.AddListener<BallDestroyedEvent>(BallDestroyed);
        }

        private void BallDestroyed(BallDestroyedEvent obj)
        {
            GameRules.Balls.RemoveAt(0);
            CheckLoose();
        }

        private void LoadGameRules()
        {
            GenerateGameRules();
            CreateBorders();
        }
        private void CreateBorders()
        {
            ViewGenerator.CreateObjectFromResouces("Borders");
        }

        private void GenerateGameRules()
        {
            GameRules = new GameRules();
            int brickId = 0;
            for (int i = 0; i < DefaultHight; i++)
            {
                for (int j = 0; j < DefaultWidth; j++)
                {
                    GameRules.Bricks.Add(new Brick()
                    {
                        Position = new Vector2(j,i+5),
                        ID = brickId,
                        Health = i+1,
                    });

                    brickId++;

                    if ((j % 2 == 0 && i % 3 == 1) || i == 0)
                    {
                        GameRules.Bricks[brickId -1].BrakeEffects.Add(new AddBallsEffect(){Count = 2});
                    }
                    if ((i % 2==1))
                    {
                        GameRules.Bricks[brickId - 1].Bonuses.Add(new BrickSizeBonus() { Value = j % 3 == 0 ? -2 : 2 });
                    }
                }
            }
        }

        private void CheckLoose()
        {
            bool loose = GameRules.GameStarted && GameRules.Balls.Count == 0;
            if (loose)
            {
                SceneManager.LoadScene(0);
            }
        }

    }
}
