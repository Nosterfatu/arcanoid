﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Data;
using Assets.Scripts.Data.EventData;
using Assets.Scripts.Managers;
using Assets.Scripts.Utils;
using Assets.Scripts.View;
using UnityEngine;

public class BrickManager 
{
    private Dictionary<int, Brick> _bricks = new Dictionary<int, Brick>();
    private Dictionary<int, BrickView> _brickViews = new Dictionary<int, BrickView>();
    private GameRulesManager _gameRulesManager;
    private const string HolderName = "BrickHolder";
    private Transform _holder;
    public BrickManager(GameRulesManager gameRulesManager)
    {
        _gameRulesManager = gameRulesManager;
        GenerateBricks(gameRulesManager.GameRules.Bricks);
        EventAggregator.AddListener<BrickHittedEvent>(OnBrickHit);
    }

    public void Unsubscribe()
    {
        EventAggregator.RemoveListener<BrickHittedEvent>(OnBrickHit);
    }

    private void GenerateBricks(List<Brick> gameRulesBricks)
    {
        _holder = ViewGenerator.CreateHolder(HolderName);
        foreach (var brick in gameRulesBricks)
        {
            _bricks.Add(brick.ID, brick);
            var view = ViewGenerator.CreateView<BrickView>(brick.PrefabName, brick.Position, _holder);
            _brickViews.Add(brick.ID, view);
            SetupView(brick, view);
        }
    }

    private void SetupView(Brick brick, BrickView view)
    {
        view.SetHealth(brick.Health);
        var color = new Color32((byte)brick.Health,(byte)(brick.Position.x *10), (byte)(brick.Position.y * 10),255);
        view.SetColor(color);
        view.SetID(brick.ID);
    }

    private void OnBrickHit(BrickHittedEvent data)
    {
        var brick = _bricks[data.ID];
        brick.Health--;
        foreach (var effect in brick.HitEffects)
        {
            effect.MakeEffect(brick);
        }

        if (brick.Health <= 0)
        {
            foreach (var effect in brick.BrakeEffects)
            {
                effect.MakeEffect(brick);
            }

            foreach (var bonus in brick.Bonuses)
            {
                var view = ViewGenerator.CreateView<BonusView>(bonus.PrefabName, brick.Position, null);
                view.Bonus = bonus;
                view.SetBonusText(bonus.Text, bonus.Color);
            }

            data.View.Destroy();
        }
        else
        {
            UpdateView(brick,data.View);
        }
    }

    private void UpdateView(Brick brick, BrickView view)
    {
        view.SetHealth(brick.Health);
    }
}
