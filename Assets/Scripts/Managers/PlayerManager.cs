﻿using Assets.Scripts.Data;
using Assets.Scripts.Data.EventData;
using Assets.Scripts.Utils;
using Assets.Scripts.View;
using UnityEngine;

namespace Assets.Scripts.Managers
{
    public class PlayerManager 
    {
        private const float PlayerStartPosition =  -10;
        private const float GameCenter = 7;

        private PlayerView _playerView;
        private Player _player;
        private BallView _handledBallView;

        private GameRulesManager _rulesManager;
        private InputManager _inputManager;

        public PlayerManager(GameRulesManager rulesManager, InputManager inputManager)
        {
            _rulesManager = rulesManager;
            _inputManager = inputManager;
            GeneratePlayer();
            Subscribe();
        }

        private void Subscribe()
        {
            EventAggregator.AddListener<GameStartedEvent>(OnGameStarted);
            EventAggregator.AddListener<CreateBallsEvent>(OnBallsCreateCall);
            EventAggregator.AddListener<PlayerSizeChangedEvent>(OnPlayerSizeChanged);
            EventAggregator.AddListener<BonusCatchedEvent>(OnBonusCatched);
        }

        private void OnBonusCatched(BonusCatchedEvent obj)
        {
            obj.Bonus.ApplyBonus(_player);
        }

        private void OnPlayerSizeChanged(PlayerSizeChangedEvent obj)
        {
            _playerView.SetSize(_player.Size);
        }

        private void OnBallsCreateCall(CreateBallsEvent obj)
        {
            for (int i = 0; i < obj.Count; i++)
            {
                var ball = new Ball();
                _rulesManager.GameRules.Balls.Add(ball);
                var view = ViewGenerator.CreateView<BallView>(ball.PrefabName,  obj.Position, null);
                float randomDirection = 0;
                while (Mathf.Abs(randomDirection)< 1)
                {
                    randomDirection = Random.Range(-5, 5);
                }
                view.Run(new Vector2(_playerView.transform.position.x - GameCenter, randomDirection).normalized * 10);
            }
        }

        public void Unsubscribe()
        {
            EventAggregator.RemoveListener<GameStartedEvent>(OnGameStarted);
            EventAggregator.RemoveListener<CreateBallsEvent>(OnBallsCreateCall);
            EventAggregator.RemoveListener<PlayerSizeChangedEvent>(OnPlayerSizeChanged);
            EventAggregator.RemoveListener<BonusCatchedEvent>(OnBonusCatched);

        }

        private void OnGameStarted(GameStartedEvent obj)
        {
            _handledBallView.Run(new Vector2(_playerView.transform.position.x - GameCenter, 1).normalized *10);
            _handledBallView = null;
            _rulesManager.GameRules.GameStarted = true;
        }

        private void GeneratePlayer()
        {
            _player = _rulesManager.GameRules.Player;
            _playerView = ViewGenerator.CreateView<PlayerView>(_player.PrefabName, new Vector2(GameCenter, PlayerStartPosition), null);
            var ball = new Ball();
            _rulesManager.GameRules.Balls.Add(ball);
            _handledBallView = ViewGenerator.CreateView<BallView>(ball.PrefabName, new Vector2(GameCenter, PlayerStartPosition+1), null);
        }

        public void OnUpdate()
        {
            Move();
        }

        private void Move()
        {
            if (_inputManager.HasLeft)
            {
                float velocity = CalculateMove(_playerView.GetPosition() -(_player.Size / 2f), GameRulesManager.LeftBorderPosition, -_player.Speed * Time.deltaTime);
                _playerView.SetMove(velocity);
            }
            else if (_inputManager.HasRight)
            {
                float velocity = CalculateMove((_player.Size / 2f)+ _playerView.GetPosition(), GameRulesManager.RightBorderPosition, _player.Speed * Time.deltaTime);
                _playerView.SetMove(velocity);
            }
        }

        private float CalculateMove(float breaksPosition, float border, float velocity)
        {
            var diff = Mathf.Abs(breaksPosition - border);
            var velocityAbs = Mathf.Abs(velocity);
            if (diff < velocityAbs)
            {
                return 0;
            }

            return velocity;
        }
    }
}
