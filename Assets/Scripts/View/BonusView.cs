﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.View
{
    public class BonusView : MonoBehaviour, ISimpleDestroy
    {
        [SerializeField] private Text _bonusText;
        public BrickBonus Bonus { get; set; }

        public void SetBonusText(string text, Color color)
        {
            _bonusText.text = text;
            _bonusText.color = color;
        }
        public void Destroy()
        {
            Destroy(gameObject);
        }
    }
}
