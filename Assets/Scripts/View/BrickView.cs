﻿using Assets.Scripts.Data.EventData;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Assets.Scripts.View
{
    public class BrickView : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField] private Text _healthText;
        [SerializeField] private SpriteRenderer _renderer;
        [SerializeField] private int _ID;

        public void SetHealth(int brickHealth)
        {
            _healthText.text = brickHealth > 1 ? brickHealth.ToString() : "";
        }

        public void SetColor(Color32 color)
        {
            _renderer.color = color;
        }

        public void SetID(int brickId)
        {
            _ID = brickId;
        }

        public void Destroy()
        {
            Destroy(gameObject);
        }

        public void BrickHitted()
        {
            EventAggregator.SendMassage(new BrickHittedEvent(){ID = _ID, View = this});
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            BrickHitted();
        }
    }
}
