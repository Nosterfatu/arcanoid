﻿using Assets.Scripts.Data.EventData;
using UnityEngine;

namespace Assets.Scripts.View
{
    public class PlayerView : MonoBehaviour
    {
        public void SetMove(float speed)
        {
            transform.position += new Vector3(speed,0);
        }

        public void SetSize(float size)
        {
            transform.localScale = new Vector3(size,1,1);
        }

        public float GetPosition()
        {
            return transform.position.x;
        }
  
        private void OnTriggerEnter2D(Collider2D collision)
        {
            var bonusView = collision.GetComponent<BonusView>();
            EventAggregator.SendMassage(new BonusCatchedEvent() {Bonus = bonusView.Bonus});
            bonusView.Destroy();
        }
    }
}

