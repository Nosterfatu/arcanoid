﻿using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Assets.Scripts.View
{
    public class PressingButton : Button
    {
        public UnityAction ButtonPressAction;
        public UnityAction ButtonUpAction;
        public override void OnPointerDown(PointerEventData eventData)
        {
            base.OnPointerDown(eventData);
            ButtonPressAction?.Invoke();
        }

        public override void OnPointerUp(PointerEventData eventData)
        {
            base.OnPointerUp(eventData);
            ButtonUpAction?.Invoke();
        }
    }
}