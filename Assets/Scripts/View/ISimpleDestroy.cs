﻿namespace Assets.Scripts.View
{
    public interface ISimpleDestroy
    {
        void Destroy();
    }
}