﻿using UnityEngine;

namespace Assets.Scripts.View
{
    public class DeadLineView : MonoBehaviour
    {
        private void OnCollisionEnter2D(Collision2D collision)
        {
            var simpleDestroy = collision.gameObject.GetComponent<ISimpleDestroy>();
            simpleDestroy?.Destroy();
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            var simpleDestroy = collision.GetComponent<ISimpleDestroy>();
            simpleDestroy?.Destroy();
        }
    }
}
