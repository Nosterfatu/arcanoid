﻿using Assets.Scripts.Data.EventData;
using UnityEngine;

namespace Assets.Scripts.View
{
    public class BallView : MonoBehaviour, ISimpleDestroy
    {
        [SerializeField]
        private Rigidbody2D _rigidbody2d;

        private Vector2 _originForce;

        public void Run(Vector2 force)
        {
            _rigidbody2d.AddForce(force, ForceMode2D.Impulse);
            _originForce = force;
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            var brick = collision.gameObject.GetComponent<BrickView>();
            brick?.BrickHitted();
        }

        public void Destroy()
        {
            Destroy(gameObject);
            EventAggregator.SendMassage(new BallDestroyedEvent());
        }

    }
}
