﻿using Assets.Scripts.View;

namespace Assets.Scripts.Data.EventData
{
    public class BrickHittedEvent : EventData
    {
        public int ID;
        public BrickView View;
    }
}