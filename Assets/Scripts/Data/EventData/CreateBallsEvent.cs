﻿using UnityEngine;

namespace Assets.Scripts.Data.EventData
{
    public class CreateBallsEvent : EventData
    {
        public int Count;
        public Vector2 Position { get; set; }
    }
}