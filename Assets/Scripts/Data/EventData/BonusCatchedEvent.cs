﻿namespace Assets.Scripts.Data.EventData
{
    public class BonusCatchedEvent : EventData
    {
        public BrickBonus Bonus { get; set; }
    }
}