﻿using System;
using System.Collections.Generic;

namespace Assets.Scripts.Data.EventData
{
    public class EventListener<T> where T : EventData
    {
        public List<Action<T>> Actions = new List<Action<T>>();

        public void HandleMassage(T massage)
        {
            for (int i = 0; i < Actions.Count; i++)
            {
                Actions[i]?.Invoke(massage);
            }
        }
    }
}