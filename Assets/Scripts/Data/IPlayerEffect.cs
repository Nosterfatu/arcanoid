﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPlayerEffect 
{ 
    bool IsPermanent { get; }

    int Value { get; set; }
    PlayerEffectType EffectType { get; }
}

public enum PlayerEffectType
{
    None = 0,
    Resizer = 1
}