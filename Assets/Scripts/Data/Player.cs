﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player
{
    public List<IPlayerEffect> Effects= new List<IPlayerEffect>();
    public int Lifes = 3;
    public int Size = 5;
    public int Speed = 10;
    public string PrefabName => "Player";
}
