﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Data
{
    public class Brick
    {
        public int ID;
        public int Health = 1;
        public int BrickScore;
        public Vector2 Position;
        public List<BrickBonus> Bonuses = new List<BrickBonus>();
        public List<IBrickEffect> HitEffects = new List<IBrickEffect>();
        public List<IBrickEffect> BrakeEffects = new List<IBrickEffect>();
        public string PrefabName => "Brick";
    }
}
