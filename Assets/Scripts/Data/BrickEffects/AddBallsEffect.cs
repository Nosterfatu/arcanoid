﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Data;
using Assets.Scripts.Data.EventData;
using UnityEngine;

public class AddBallsEffect : IBrickEffect
{
    public int Count;
    public void MakeEffect(Brick brick)
    {
        EventAggregator.SendMassage(new CreateBallsEvent()
        {
            Count = Count,
            Position = brick.Position
        } );
    }
}
