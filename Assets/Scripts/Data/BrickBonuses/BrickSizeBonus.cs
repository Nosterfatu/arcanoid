﻿using System.Linq;
using Assets.Scripts.Data.EventData;
using Assets.Scripts.Managers;
using UnityEngine;

namespace Assets.Scripts.Data.BrickBonuses
{
    public class BrickSizeBonus : BrickBonus , IPlayerEffect
    {
        public PlayerEffectType EffectType => PlayerEffectType.Resizer;
        public bool IsPermanent => true;
        public int Value { get; set; }
        public override string PrefabName => "Bonus";
        public override Color Color => Value > 0 ? Color.green : Color.red;
        public override string Text => (Value > 0? "+" : "-") + $"{Value}";

        public override void ApplyBonus(Player player)
        {
           var effect = player.Effects.FirstOrDefault(e => e.EffectType == EffectType);
           if (effect != null)
           {
               ((BrickSizeBonus) effect).ApplyValue(Value, player);
           }
           else
           {
               player.Effects.Add(this);
               ApplyValue(Value, player);
           }
        }

        private void ApplyValue(int value, Player player)
        {
            player.Size += value;

            if (player.Size > GameRulesManager.PlayerMaxSize)
            {
                player.Size = GameRulesManager.PlayerMaxSize;
            }
            else if (player.Size < GameRulesManager.PlayerMinSize)
            {
                player.Size = GameRulesManager.PlayerMinSize;
            }

            EventAggregator.SendMassage(new PlayerSizeChangedEvent());
        }
    }
}
