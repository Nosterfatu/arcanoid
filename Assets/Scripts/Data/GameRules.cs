﻿
using System.Collections.Generic;

namespace Assets.Scripts.Data
{
    public class GameRules
    {
        public List<Brick> Bricks = new List<Brick>();
        public Player Player = new Player();
        public List<Ball> Balls = new List<Ball>();
        public bool GameStarted;
    }
}