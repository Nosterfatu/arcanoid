﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BrickBonus
{
    public abstract string PrefabName { get; }
    public virtual Color Color => Color.white;
    public virtual string Text => "";

    public abstract void ApplyBonus(Player player);
}
