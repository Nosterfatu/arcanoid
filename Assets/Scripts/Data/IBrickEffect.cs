﻿namespace Assets.Scripts.Data
{
    public interface IBrickEffect
    {
        void MakeEffect(Brick brick);
    }
}
